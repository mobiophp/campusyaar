<?php

class Mobio_Bulkinquiry_Block_Adminhtml_Bulkinquiry_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('bulkinquiryGrid');
        $this->setDefaultSort('bulk_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('bulkinquiry/bulkinquiry')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('bulk_id', array(
            'header' => Mage::helper('bulkinquiry')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'bulk_id',
        ));


        $this->addColumn('name', array(
            'header' => Mage::helper('bulkinquiry')->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));


        $this->addColumn('state', array(
            'header' => Mage::helper('bulkinquiry')->__('State'),
            'align' => 'left',
            'index' => 'state',
        ));

        $this->addColumn('city', array(
            'header' => Mage::helper('bulkinquiry')->__('City'),
            'align' => 'left',
            'index' => 'city',
            'width' => '150',
        ));

        $this->addColumn('contact_no', array(
            'header' => Mage::helper('bulkinquiry')->__('Contact No'),
            'align' => 'left',
            'index' => 'contact_no',
            'width' => '100',
        ));

        $this->addColumn('email_id', array(
            'header' => Mage::helper('bulkinquiry')->__('Email Id'),
            'align' => 'left',
            'index' => 'email_id',
        ));


        $this->addColumn('total_qty', array(
            'header' => Mage::helper('bulkinquiry')->__('Total Qty'),
            'align' => 'center',
            'index' => 'total_qty',
            'width' => '70',
            'filter' => false,
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('bulkinquiry')->__('Date'),
            'align' => 'left',
            'index' => 'created_at',
            'width' => '130',
            'filter' => false,
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('bulkinquiry')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('bulkinquiry')->__('View'),
                    'url' => array('base' => '*/*/view'),
                    'field' => 'id'
                ),
                array(
                    'caption' => Mage::helper('bulkinquiry')->__('Delete'),
                    'url' => array('base' => '*/*/delete'),
                    'field' => 'id',
                    'confirm' => Mage::helper('bulkinquiry')->__('Are you sure?')
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));




        //	$this->addExportType('*/*/exportCsv', Mage::helper('banners')->__('CSV'));
        //	$this->addExportType('*/*/exportXml', Mage::helper('banners')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('bulk_id');
        $this->getMassactionBlock()->setFormFieldName('bulk');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('bulkinquiry')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('bulkinquiry')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/view', array('id' => $row->getId()));
    }

}
