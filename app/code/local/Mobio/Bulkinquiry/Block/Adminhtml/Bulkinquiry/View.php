<?php

class Mobio_Bulkinquiry_Block_Adminhtml_Bulkinquiry_View extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function getBulkinquiry() {
        $id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('bulkinquiry/bulkinquiry')->getCollection();
        $collection->addFieldToFilter('bulk_id', $id);
        $extrabrand = $collection->getFirstItem();
        return $extrabrand->getData();
    }

    public function getHeaderText() {
        return Mage::helper('bulkinquiry')->__('View Bulk Inquiry');
    }

    public function getButtonsHtml() {
        $data = array(
            'label' => 'Back',
            'onclick' => 'setLocation(\'' . $this->getUrl('*/*/*') . '\')',
            'class' => 'back'
        );
        return $this->addButton('Back', $data, 0, 100, 'header');
    }

}
