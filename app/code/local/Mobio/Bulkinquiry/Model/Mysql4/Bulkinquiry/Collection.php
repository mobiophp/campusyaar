<?php

class Mobio_Bulkinquiry_Model_Mysql4_Bulkinquiry_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('bulkinquiry/bulkinquiry');
    }
}