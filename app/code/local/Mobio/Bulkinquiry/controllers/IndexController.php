<?php

class Mobio_Bulkinquiry_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
        // echo Mage::getSingleton('core/design_package')->getTheme('frontend');
    }

    public function postAction() {

        $post = $this->getRequest()->getPost();

        if ($post) {
            try {
                $dataArray = array();

                $dataArray['product_type'] = 1;
                $dataArray['product_option'] = implode($post['product_options']);
                //$dataArray['product_color'] = implode($post['product_color']);
                $dataArray['product_color'] = "";
                $totalQty = 0;
                if (isset($post['product_sizes'])) {
                    foreach ($post['product_sizes'] as $keys => $values) {
                        if ($keys != "total") {
                            $totalQty += $values['qty'];
                        }
                    }
                }

                $dataArray['product_sizes'] = json_encode($post['product_sizes']);
                $dataArray['total_qty'] = $totalQty;
                $dataArray['product_addition'] = json_encode($post['product_addition']);
                $dataArray['information'] = $post['information'];
                $dataArray['name'] = $post['name'];
                $dataArray['email_id'] = $post['email_id'];
                $dataArray['state'] = $post['state'];
                $dataArray['city'] = $post['city'];
                $dataArray['contact_no'] = $post['phone_number'];
                $dataArray['delivery_location'] = $post['delivery_location'];
                $dataArray['message'] = $post['message'];
                $dataArray['created_at'] = date('Y-m-d H:i:s');

                $pathLogo = Mage::getBaseDir("media") . DS . "Bulkinquiry" . DS;
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $AttachmentArray = $this->reArrayFiles($_FILES['product_attachment']);
                $uploadedFile = array();
                foreach ($AttachmentArray as $key => $values) {

                    $logo = new Varien_File_Uploader(
                            array(
                        'name' => $_FILES['product_attachment']['name'][$key],
                        'type' => $_FILES['product_attachment']['type'][$key],
                        'tmp_name' => $_FILES['product_attachment']['tmp_name'][$key],
                        'error' => $_FILES['product_attachment']['error'][$key],
                        'size' => $_FILES['product_attachment']['size'][$key]
                            )
                    );

                    $logo->setAllowedExtensions($allowedExts);
                    $logo->setAllowRenameFiles(true);
                    $logo->setFilesDispersion(false);
                    if (!is_dir($pathLogo)) {
                        mkdir($pathLogo, 0777, true);
                    }

                    $temp = explode(".", $_FILES['product_attachment']['name'][$key]);
                    $newfilename = round(microtime(true)) . '_' . $temp[0] . '.' . end($temp);
                    $logo->save($pathLogo, $newfilename);
                    $filename = $logo->getUploadedFileName();
                    $uploadedFile[] = $filename;
                }
                $dataArray['product_attachment'] = json_encode($uploadedFile);
                $BulkinquiryModel = Mage::getModel('bulkinquiry/bulkinquiry')->setData($dataArray);
                $insertId = $BulkinquiryModel->save()->getId();

                $emailTemplate = Mage::getModel('core/email_template')
                        ->loadDefault('bulk_user_email_template');


                $emailTemplateVariables = array();
                $emailTemplateVariables['cust_name'] = $post['name'];
                $emailTemplateVariables['cust_email_id'] = $post['email_id'];
                $emailTemplateVariables['cust_phone_number'] = $post['phone_number'];
                $emailTemplateVariables['message'] = $post['message'];
                $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
                //$emailTemplate->send($post['quote_email_id'],$post['quote_name'], $emailTemplateVariables);

                $admin_email = Mage::getStoreConfig('trans_email/ident_general/email');
                $admin_name = Mage::getStoreConfig('trans_email/ident_general/name');

                $mail = Mage::getModel('core/email')
                        ->setToName($post['name'])
                        ->setToEmail($post['email_id'])
                        ->setBody($processedTemplate)
                        ->setSubject('Subject :')
                        ->setFromEmail($admin_email)
                        ->setFromName($admin_name)
                        ->setType('html');

                //$mail->send();


                $adminemailTemplate = Mage::getModel('core/email_template')
                        ->loadDefault('bulk_admin_email_template');
                $adminprocessedTemplate = $adminemailTemplate->getProcessedTemplate($emailTemplateVariables);
                //$adminemailTemplate->send($post['quote_email_id'],$post['quote_name'], $emailTemplateVariables);

                $adminmail = Mage::getModel('core/email')
                        ->setToName($admin_name)
                        ->setToEmail($admin_email)
                        ->setBody($adminprocessedTemplate)
                        ->setSubject('Subject :')
                        ->setType('html');

                //$adminmail->send();

                Mage::getSingleton('core/session')->addSuccess(Mage::helper('bulkinquiry')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {

                Mage::getSingleton('core/session')->addError(Mage::helper('bulkinquiry')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    protected function reArrayFiles(&$file_post) {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);
        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
}