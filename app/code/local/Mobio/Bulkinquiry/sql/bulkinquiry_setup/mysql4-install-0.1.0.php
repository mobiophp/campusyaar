<?php

$installer = $this;

$installer->startSetup();


$table = $installer->getConnection()
        ->newTable($installer->getTable('bulkinquiry/bulkinquiry'))
        ->addColumn('bulk_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Bulk Id')
        ->addColumn('product_type', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
            'unsigned' => true,
            'nullable' => false,
                ), '1:T-shirt,2:cap,3:Hoodie,4:Trousers')
        ->addColumn('product_option', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'nullable' => false,
            'default' => null,
                ), 'Product Type Option')
        ->addColumn('product_color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'nullable' => true,
            'default' => null,
                ), 'Product Colors')
        ->addColumn('product_sizes', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'nullable' => false,
            'default' => null,
                ), 'Product Sizes and Qty')
        ->addColumn('total_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
            'nullable' => false,
            'default' => 0,
                ), 'Total Qty')
        ->addColumn('product_addition', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'nullable' => true,
            'default' => null,
                ), 'Product Addition Detail')
        ->addColumn('information', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'nullable' => true,
            'default' => null,
                ), 'Product Information')
        ->addColumn('product_attachment', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'nullable' => false,
            'default' => null,
                ), 'Product Attachment ')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Contact Name')
        ->addColumn('email_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 200, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Contact Email Id')
        ->addColumn('state', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'nullable' => false,
            'default' => '0',
                ), 'Contact State')
        ->addColumn('city', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Contact City')
        ->addColumn('contact_no', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Contact No')
        ->addColumn('delivery_location', Varien_Db_Ddl_Table::TYPE_VARCHAR, 400, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Delivery Location')
        ->addColumn('message', Varien_Db_Ddl_Table::TYPE_VARCHAR, 1000, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Contact Message')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 100, array(
            'unsigned' => false,
            'nullable' => false,
                ), 'Created At')
        ->setComment('Bilk Inquiry table store the information about the inquiry.');
$installer->getConnection()->createTable($table);
$installer->endSetup();
?>