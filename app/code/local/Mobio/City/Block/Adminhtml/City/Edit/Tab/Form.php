<?php

class Mobio_City_Block_Adminhtml_City_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('city_form', array('legend'=>Mage::helper('city')->__('Item information')));

            //$object = Mage::getModel('city/city')->load( $this->getRequest()->getParam('id') );
            //$imgPath = Mage::getBaseUrl('media')."Banners/images/thumb/".$object['bannerimage'];

        $fieldset->addField('city_name', 'text', array(
            'label'     => Mage::helper('city')->__('City Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'city_name',
        ));
        $fieldset->addField('city_order', 'text', array(
            'label'     => Mage::helper('city')->__('Sort Order'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'city_order',
        ));
        
        $fieldset->addField('city_status', 'select', array(
            'label'     => Mage::helper('city')->__('Status'),
            'name'      => 'city_status',
            'values'    => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('city')->__('Enabled'),
                ),

                array(
                    'value'     => 2,
                    'label'     => Mage::helper('city')->__('Disabled'),
                ),
            ),
        ));
     
      if ( Mage::getSingleton('adminhtml/session')->getBannersData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBannersData());
          Mage::getSingleton('adminhtml/session')->setBannersData(null);
      } elseif ( Mage::registry('city_data') ) {
          $form->setValues(Mage::registry('city_data')->getData());
      }
      return parent::_prepareForm();
  }
}