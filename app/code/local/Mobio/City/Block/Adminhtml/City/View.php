<?php

class Mobio_City_Block_Adminhtml_City_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function getCity(){
        $id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('city/city')->getCollection();
        $collection->addFieldToFilter('city_id', $id);
        $extrabrand = $collection->getFirstItem();
        return $extrabrand->getData();
    }
    
    public function getHeaderText()
    {
        return Mage::helper('city')->__('View City');
    }
    
    public function getButtonsHtml(){
        $data = array(
            'label' =>  'Back',
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/*') . '\')',
            'class'     =>  'back'
        );
        return $this->addButton ('Back', $data, 0, 100,  'header'); 
    }
    
}