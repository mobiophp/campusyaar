<?php

class Mobio_City_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 0;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('adminhtml')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('adminhtml')->__('Disabled')
        );
    }
    
    public function getCities()     
    { 
       
        $city_name = array(
          1 => "Ahmedabad",
          2 => "Surat",
          3 => "Baroda", 
          4 => "Rajkot", 
          5 => "Jaypur" 
        );
        
        return $city_name;
    }
    
}