<?php

$installer = $this;

$installer->startSetup();


$installer->run("
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (                                  
		   `city_id` int(11) unsigned NOT NULL auto_increment,  
		   `city_name` varchar(255) NOT NULL default '' Unique,               
		   `city_order` smallint(6) NOT NULL default '1',
                   `city_status` smallint(6) NOT NULL default '1',              
		   PRIMARY KEY  (`city_id`)                             
		 ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

");
$installer->endSetup(); 

?>