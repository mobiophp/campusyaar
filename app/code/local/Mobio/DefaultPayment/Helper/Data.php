<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 *
 * @author root
 */
class Mobio_DefaultPayment_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getActivePaymentMethods() {

        $payments = Mage::getSingleton('payment/config')->getActiveMethods();
        $methods = array();
        foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
            if ($paymentCode != 'free' && $paymentCode != 'paypal_mecl' && $paymentCode != 'paypal_billing_agreement') {
                $methods[$paymentCode] = array(
                    'title' => $paymentTitle,
                    'code' => $paymentCode
                );
            }
        }
        return $methods;
    }

    public function _getQuote($quoteId)
    {
        return Mage::getModel('sales/quote')->load($quoteId);
    }
}
