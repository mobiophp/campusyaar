<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 *
 * @author root
 */
class Mobio_DefaultPayment_Model_Observer {

    public function setDefaultPaymentMethod(Varien_Event_Observer $obs) {
        if (Mage::getStoreConfig('defaultpayment/payment_configuration/payment_status')) {
            $methodCode = Mage::getStoreConfig('defaultpayment/payment_configuration/payment_methods');
            if ($methodCode != "") {
                $quote = Mage::getSingleton('checkout/session')->getQuote();
                $quote->getPayment()->importData(array('method' => $methodCode));
            }
        }
    }
}