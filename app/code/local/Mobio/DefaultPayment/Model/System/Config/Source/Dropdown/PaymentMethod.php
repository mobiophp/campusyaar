<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 *
 * @author root
 */
class Mobio_DefaultPayment_Model_System_Config_Source_Dropdown_PaymentMethod {

    public function toOptionArray() {

        $activePaymentMethod = Mage::helper('defaultpayment')->getActivePaymentMethods();
        $paymentArray = array();
        foreach ($activePaymentMethod as $key => $values) {
            $paymentArray[$values['code']] = array(
                'value' => $values['code'],
                'label' => $values['title'],
            );
        }
        return $paymentArray;
    }
}