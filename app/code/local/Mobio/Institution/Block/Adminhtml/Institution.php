<?php
class Mobio_Institution_Block_Adminhtml_Institution extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_institution';
    $this->_blockGroup = 'institution';
    $this->_headerText = Mage::helper('institution')->__('Institution Manager');
    $this->_addButtonLabel = Mage::helper('institution')->__('Add Institution');
	
    parent::__construct();
    $this->_removeButton('add');
  }
}