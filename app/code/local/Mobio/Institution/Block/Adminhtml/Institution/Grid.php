<?php

class Mobio_Institution_Block_Adminhtml_Institution_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('institutionGrid');
      $this->setDefaultSort('institution_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('institution/institution')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('institution_id', array(
          'header'    => Mage::helper('institution')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'institution_id',
      ));

	
      $this->addColumn('institution_name', array(
          'header'    => Mage::helper('institution')->__('Institute Name'),
          'align'     =>'left',
          'index'     => 'institution_name',
      ));
      $city_name = Mage::getSingleton('institution/status')->getCities();
      $this->addColumn('city_name', array(
          'header'    => Mage::helper('institution')->__('City'),
          'align'     => 'left',
          'width'     => '150px',
          'index'     => 'city_name',
          'type'      => 'options',
          'options'   => $city_name,
      ));
      
      
      $this->addColumn('institution_phone_number', array(
          'header'    => Mage::helper('institution')->__('Institute Contact Number'),
          'align'     =>'left',
          'index'     => 'institution_phone_number',
      ));
      $this->addColumn('institution_email_id', array(
          'header'    => Mage::helper('institution')->__('Institute Email Id'),
          'align'     =>'left',
          'index'     => 'institution_email_id',
      ));
	  
      
      $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('institution')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('institution')->__('View'),
                        'url'       => array('base'=> '*/*/view'),
                        'field'     => 'id'
                    ),
                    array(
                        'caption'   => Mage::helper('institution')->__('Delete'),
                        'url'       => array('base'=> '*/*/delete'),
                        'field'     => 'id',
                        'confirm'  => Mage::helper('institution')->__('Are you sure?')
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
      
      
        
      
	//	$this->addExportType('*/*/exportCsv', Mage::helper('banners')->__('CSV'));
	//	$this->addExportType('*/*/exportXml', Mage::helper('banners')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('institution_id');
        $this->getMassactionBlock()->setFormFieldName('institution');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('institution')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('institution')->__('Are you sure?')
        ));

        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/view', array('id' => $row->getId()));
  }

}