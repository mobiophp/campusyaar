<?php

class Mobio_Institution_Block_Adminhtml_Institution_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function getInstitution(){
        $id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('institution/institution')->getCollection();
        $collection->addFieldToFilter('institution_id', $id);
        $extrabrand = $collection->getFirstItem();
        return $extrabrand->getData();
    }
    
    public function getHeaderText()
    {
        return Mage::helper('institution')->__('View Institute');
    }
    
    public function getButtonsHtml(){
        $data = array(
            'label' =>  'Back',
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/*') . '\')',
            'class'     =>  'back'
        );
        return $this->addButton ('Back', $data, 0, 100,  'header'); 
    }
    
}