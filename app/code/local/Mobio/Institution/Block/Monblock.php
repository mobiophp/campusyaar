<?php
class Mobio_Institution_Block_Monblock extends Mage_Core_Block_Template
{
    
    public function getBanners()     
    { 
        if (!$this->hasData('banners')) {
            $this->setData('banners', Mage::registry('banners'));
        }
        return $this->getData('banners');
        
    }
    
    public function getCities()     
    { 
       
        return Mage::getSingleton('institution/status')->getCities();
    }
    
}