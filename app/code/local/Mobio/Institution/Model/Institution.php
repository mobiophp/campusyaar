<?php

class Mobio_Institution_Model_Institution extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('institution/institution');
    }
}