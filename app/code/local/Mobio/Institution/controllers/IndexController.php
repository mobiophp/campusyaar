<?php

class Mobio_Institution_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
        // echo Mage::getSingleton('core/design_package')->getTheme('frontend');
    }

    public function postAction() {
        $post = $this->getRequest()->getPost();
        $post['created_time'] = date('Y-m-d H:i:s');

        if ($post) {


            try {

                $ins_name = $post['institution_name'];
                $ins_email = $post['institution_email_id'];
                $city = $post['city_name'];

                $institutionData = Mage::getModel('institution/institution')
                        ->getCollection()
                        ->addFieldToFilter("institution_name", $ins_name)
                        ->addFieldToFilter("institution_email_id", $ins_email)
                        ->addFieldToFilter("city_name", $city)
                        ->getData();

                if (count($institutionData) > 0) {
                    Mage::getSingleton('core/session')->addError(Mage::helper('institution')->__('Store already registered with this "' . $ins_email . '". Please contact to admin.'));
                    $this->_redirect('*/*/');
                    return;
                } else {

                    $model = Mage::getModel('institution/institution')->setData($post);
                    $insertId = $model->save()->getId();


                    $emailTemplate = Mage::getModel('core/email_template')
                            ->loadDefault('institution_user_email_template');
                    $city_name = Mage::getSingleton('institution/status')->getCities();
                    $emailTemplateVariables = array();
                    $emailTemplateVariables['institution_name'] = $post['institution_name'];
                    $emailTemplateVariables['city_name'] = $city_name[$post['city_name']];
                    $emailTemplateVariables['institution_phone_number'] = $post['institution_phone_number'];
                    $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
                    //$emailTemplate->send($post['institution_email_id'],$post['institution_name'], $emailTemplateVariables);
                    $admin_email = Mage::getStoreConfig('trans_email/ident_general/email');
                    $admin_name = Mage::getStoreConfig('trans_email/ident_general/name');

                    $mail = Mage::getModel('core/email')
                                ->setToName($post['institution_name'])
                                ->setToEmail($post['institution_email_id'])
                                ->setBody($processedTemplate)
                                ->setSubject('Subject :')
                                ->setFromEmail($admin_email)
                                ->setFromName($admin_name)
                                ->setType('html');

                    $mail->send();


                    $adminemailTemplate  = Mage::getModel('core/email_template')
                                                ->loadDefault('institution_admin_email_template');
                    $adminprocessedTemplate = $adminemailTemplate->getProcessedTemplate($emailTemplateVariables);
                    //$adminemailTemplate->send($post['institution_email_id'],$post['institution_name'], $emailTemplateVariables);

                    $adminmail = Mage::getModel('core/email')
                                ->setToName($admin_name)
                                ->setToEmail($admin_email)
                                ->setBody($adminprocessedTemplate)
                                ->setSubject('Subject :')
                                ->setType('html');

                    $adminmail->send();

                    Mage::getSingleton('core/session')->addSuccess(Mage::helper('institution')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                    $this->_redirect('*/*/');
                    return;
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(Mage::helper('institution')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }
}