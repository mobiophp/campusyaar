<?php

$installer = $this;

$installer->startSetup();


$installer->run("
DROP TABLE IF EXISTS `institution`;
CREATE TABLE `institution` (                                  
		   `institution_id` int(11) unsigned NOT NULL auto_increment,  
		   `institution_name` varchar(255) NOT NULL default '',               
		   `city_name` varchar(255) NOT NULL default '',         
		   `institution_phone_number` bigint(20) NOT NULL default '0',                                   
		   `institution_email_id` varchar(255) NOT NULL default '',
                   `committee_name` varchar(255) NULL default '',
                   `committee_phone_number` bigint(20) NULL default '0',                                   
		   `committee_email_id` varchar(255) NOT NULL default '',
                   `principal_name` varchar(255) NULL default '',
                   `principal_phone_number` bigint(20) NULL default '0',                                   
		   `principal_email_id` varchar(255) NOT NULL default '',
		   `institution_status` smallint(6) NOT NULL default '0',              
		   `created_time` datetime default NULL,                   
		   `update_time` datetime default NULL,                    
		   PRIMARY KEY  (`institution_id`)                             
		 ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

");
$installer->endSetup(); 

?>