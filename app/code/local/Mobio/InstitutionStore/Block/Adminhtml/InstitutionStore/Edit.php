<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Edit
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore_Edit extends Mage_Adminhtml_Block_Widget_Form_Container 
{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'institutionstore';
        $this->_controller = 'adminhtml_institutionstore';
        $this->_headerText = Mage::helper('institutionstore')->__('Institution Store');
         
        $this->_updateButton('save', 'label', Mage::helper('adminhtml')->__('Save Institution'));
        $this->_updateButton('delete', 'label', Mage::helper('adminhtml')->__('Delete Institution'));
        
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

//        $this->_addButton('save', array(
//            'label'     => Mage::helper('adminhtml')->__('Save'),
//            'onclick'   => 'edit_form.submit();',
//            'class'     => 'save',
//        ), 1);

        
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('institutionstore')->getId()) {
            return Mage::helper('institutionstore')->__("Edit Institution Store '%s'", $this->htmlEscape(Mage::registry('institutionstore')->getName()));
        }
        else {
            return Mage::helper('institutionstore')->__('New Institution Store');
        }
    }
}