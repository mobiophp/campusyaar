<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {
    /**
     * Init form
     */
    public function __construct() {
        parent::__construct();
        $this->setId('edit_form');
        $this->setTitle(Mage::helper('institutionstore')->__('Institution Store Information'));
        $this->setDestElementId('edit_form');
        
    }

    protected function _prepareForm() {
        
        $model = Mage::registry('institutionstore');
        $form = new Varien_Data_Form(array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))), 
                    'method' => 'post', 
                    'enctype' => 'multipart/form-data'
                    )
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}