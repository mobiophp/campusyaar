<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _construct() {
        parent::_construct();
    }

    protected function _prepareForm() {

        $model = Mage::registry('institutionstore');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('institutionstore_');

        $fieldset = $form->addFieldset('fieldset', array(
            'legend' => Mage::helper('institutionstore')->__('Store Information'),
            'class' => 'fieldset-wide')
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('name', 'text', array(
            'name' => 'institution_name',
            'label' => Mage::helper('institutionstore')->__('Name'),
            'title' => Mage::helper('institutionstore')->__('Name'),
            'width' => '250px',
            'required' => true,
        ));

        $fieldset->addField('code', 'text', array(
            'name' => 'code',
            'label' => Mage::helper('institutionstore')->__('Code'),
            'title' => Mage::helper('institutionstore')->__('Code'),
            'width' => '250px',
            'required' => true,
            'class' => "validate-alphanum-with-spaces"
        ));


        $fieldset->addField('institution_type', 'select', array(
            'name' => 'institution_type',
            'label' => Mage::helper('institutionstore')->__('Type'),
            'title' => Mage::helper('institutionstore')->__('Type'),
            'width' => '250px',
            'required' => true,
            'class' => "validate-select",
            'options' => array(
                '1' => Mage::helper('institutionstore')->__('School'),
                '2' => Mage::helper('institutionstore')->__('College'),
                '3' => Mage::helper('institutionstore')->__('Organisation'),
            ),
        ));


        if ($model->getLogo() == "") {
            $fieldset->addField('logo', 'file', array(
                'name' => 'logo',
                'label' => Mage::helper('institutionstore')->__('Logo'),
                'title' => Mage::helper('institutionstore')->__('Logo'),
                'note' => Mage::helper('institutionstore')->__('Logo Size : 120x120'),
                'required' => true
            ));
        } else {
            $path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "CY_Store/" . $model->getLogo();
            $note = 'Browse new image to replace old one.Allowed image type [ "jpg","jpeg","gif","png"]<br/><br/> <a href="' . $path . '" rel="lightbox" onclick="func_loadLightBox(this);return false;" title="' . $model->getName() . '">
                    <img src="' . $path . '" style="width:120px;height:120px;"/></a>';

            $fieldset->addField('logo', 'file', array(
                'name' => 'logo',
                'label' => Mage::helper('institutionstore')->__('Logo'),
                'title' => Mage::helper('institutionstore')->__('Logo'),
                'required' => false,
                'note' => $note
            ));
        }

        if ($model->getImage() == "") {
            $fieldset->addField('image', 'file', array(
                'name' => 'image',
                'label' => Mage::helper('institutionstore')->__('Banner'),
                'title' => Mage::helper('institutionstore')->__('Banner'),
                'required' => true,
                'note' => Mage::helper('institutionstore')->__('Banner Size : 800x350'),
            ));
        } else {
            $path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "CY_Store/" . $model->getImage();
            $note = 'Browse new image to replace old one.Allowed image type [ "jpg","jpeg","gif","png"]<br/> <br/>  <a href="' . $path . '" rel="lightbox" onclick="func_loadLightBox(this);return false;">
                    <img src="' . $path . '" style="width:250px;height:120px;"/></a>';

            $fieldset->addField('image', 'file', array(
                'name' => 'image',
                'label' => Mage::helper('institutionstore')->__('Banner'),
                'title' => Mage::helper('institutionstore')->__('Banner'),
                'required' => false,
                'note' => $note
            ));
        }

        $fieldset->addField('city', 'select', array(
            'name' => 'city',
            'label' => Mage::helper('institutionstore')->__('City'),
            'title' => Mage::helper('institutionstore')->__('City'),
            'width' => '250px',
            'required' => true,
            'options' => $this->getCities()
        ));

        $fieldset->addField('description', 'textarea', array(
            'name' => 'description',
            'label' => Mage::helper('institutionstore')->__('Description'),
            'title' => Mage::helper('institutionstore')->__('Description'),
            'required' => true
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('institutionstore')->__('Status'),
            'title' => Mage::helper('institutionstore')->__('Status'),
            'name' => 'institution_status',
            'required' => true,
            'options' => array(
                '1' => Mage::helper('institutionstore')->__('Enabled'),
                '0' => Mage::helper('institutionstore')->__('Disabled'),
            ),
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    
    protected function getCities() {

        $collection = Mage::getModel('city/city')->getCollection();
        $collection->addFieldToFilter('city_status', '1');
        $results = $collection->getData();
        if (count($results) > 0) {
            $city_name = array();
            foreach ($results as $value) {
                $city_name[$value['city_id']] = $value['city_name'];
            }
        }
        return $city_name;
    }
    
}
