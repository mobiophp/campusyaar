<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('institution_product_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('price')
                ->addAttributeToSelect('status')
                ->addAttributeToFilter('institution_store', array('eq' => 1))
                ->joinField('position', 'catalog/category_product', 'position', 'product_id=entity_id', 'category_id=' . (int) $this->getRequest()->getParam('id', 0), 'left');
        
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){

        $this->addColumn('in_products', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_products',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'entity_id',
            'width' => '50'
        ));

        $this->addColumn('entity_id', array(
            'header' => Mage::helper('adminhtml')->__('ID'),
            'sortable' => true,
            'width' => '60',
            'index' => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('adminhtml')->__('Name'),
            'index' => 'name',
            'width' => '250'
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('adminhtml')->__('SKU'),
            'width' => '80',
            'index' => 'sku'
        ));
        $this->addColumn('price', array(
            'header' => Mage::helper('adminhtml')->__('Price'),
            'type' => 'currency',
            'width' => '60',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'price'
        ));

        $this->addColumn('position', array(
            'header' => Mage::helper('catalog')->__('Position'),
            'name' => 'position',
            'width' => 20,
            'type' => 'number',
            'validate_class' => 'validate-number',
            'index' => 'position',
            'editable' => true,
            'edit_only' => true
        ));


        $this->addColumn('status', array(
            'header' => Mage::helper('catalog')->__('Status'),
            'width' => '60',
            'type' => 'status',
            'index' => 'status'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/productGrid', array('_current' => true));
    }

    protected function _addColumnFilterToCollection($column) {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _getSelectedProducts() {   // Used in grid to return selected customers values.
        $customers = array_keys($this->getSelectedProducts());
        return $customers;
    }

    public function getSelectedProducts() {
        // Product Data
        $storeId = $this->getRequest()->getParam('id');
        if (!isset($storeId)) {
            $storeId = 0;
        }

        $productIds = array();
        $dataModel = Mage::getModel("institutionstore/institutionstore")->load($storeId)->getData();
        $productId = $dataModel['product'];
        $products = array(explode(",", $productId));

        foreach ($products as $product) {
            foreach ($product as $pro) {
                $productIds[$pro] = array('position' => $pro);
            }
        }
        return $productIds;
    }

}
