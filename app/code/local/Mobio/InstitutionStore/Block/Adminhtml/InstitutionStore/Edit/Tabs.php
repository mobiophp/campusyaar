<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * admin product edit tabs
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('institutionstore_tabs');
        $this->setDestElementId('edit_form'); // this should be same as the form id define above
        $this->setTitle(Mage::helper('adminhtml')->__('Institution Information'));
    }
    
    protected function _beforeToHtml()
    {
        $this->addTab('store_section', array(
            'label'     => Mage::helper('adminhtml')->__('Store Information'),
            'title'     => Mage::helper('adminhtml')->__('Store Information'),
            'content'   => $this->getLayout()->createBlock('institutionstore/adminhtml_institutionstore_edit_tab_form')->toHtml(),
        ));
        
        $this->addTab('product_section', array(
            'label'     => Mage::helper('adminhtml')->__('Product Information'),
            'title'     => Mage::helper('adminhtml')->__('Product Information'),
            'url'       => $this->getUrl('*/*/producttab', array('_current' => true)),
            'class'     => 'ajax'
        ));
        
        return parent::_beforeToHtml();
    }
}