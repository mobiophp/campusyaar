<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grid
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore_Grid  extends Mage_Adminhtml_Block_Widget_Grid
{
    //put your code here
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('institutionstoreGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('institutionstore_filter');

    }

    

    protected function _prepareCollection()
    {
       
        $collection = Mage::getModel('institutionstore/institutionstore')->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    

    protected function _prepareColumns()
    {
        $this->addColumn('id',
            array(
                'header'=> Mage::helper('institutionstore')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'id',
        ));
        
        $this->addColumn('type',
            array(
                'header'=> Mage::helper('institutionstore')->__('Type'),
                'align'     => 'left',
                'width'     => '120px',
                'index'     => 'status',
                'type'      => 'options',
                'options'   => array(
                    1 => 'School',
                    2 => 'College',
                    3 => 'Organisation',
                )
        ));
        
        $this->addColumn('name',
            array(
                'header'=> Mage::helper('institutionstore')->__('Institution Name'),
                'index' => 'name',
        ));
        
        $this->addColumn('code',
            array(
                'header'=> Mage::helper('institutionstore')->__('Institution Code'),
                'index' => 'code',
        ));
        
        $this->addColumn('city',
            array(
                'header'=> Mage::helper('institutionstore')->__('Institution City'),
                'index' => 'city',
                'type'  => 'options',
                'options'=> $this->getCities()
            )
        );
        
        $this->addColumn('status', array(
            'header'    => Mage::helper('institutionstore')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Enabled',
                0 => 'Disabled',
            )
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('institutionstore');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> Mage::helper('institutionstore')->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
             'confirm' => Mage::helper('catalog')->__('Are you sure?')
        ));


        
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('institutionstore')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('institutionstore')->__('Status'),
                         'values' => array(
                                                1 => 'Enabled',
                                                0 => 'Disabled',
                                            )
                     )
             )
        ));

        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
            'store'=>$this->getRequest()->getParam('store'),
            'id'=>$row->getId())
        );
    }
    
    protected function getCities() {

        $collection = Mage::getModel('city/city')->getCollection();
        $collection->addFieldToFilter('city_status', '1');
        $results = $collection->getData();
        if (count($results) > 0) {
            $city_name = array();
            foreach ($results as $value) {
                $city_name[$value['city_id']] = $value['city_name'];
            }
        }
        return $city_name;
    }
    
}