<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Banner
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Adminhtml_Institutionstore  extends Mage_Adminhtml_Block_Widget_Container
{
    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('institutionstore/institutionstore.phtml');
    }
    
    protected function _prepareLayout()
    {
        $this->_addButton('add_new', array(
            'label'   => Mage::helper('institutionstore')->__('Add Institution Store'),
            'onclick' => "setLocation('{$this->getUrl('*/*/new')}')",
            'class'   => 'add'
        ));
            
        $this->setChild('grid', $this->getLayout()->createBlock('institutionstore/adminhtml_institutionstore_grid', 'institutionstore.grid'));
        return parent::_prepareLayout();
    }
    
    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_new_button');
    }
    
    
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }
    
    
    public function isSingleStoreMode()
    {
        if (!Mage::app()->isSingleStoreMode()) {
               return false;
        }
        return true;
    }
    
}

?>
