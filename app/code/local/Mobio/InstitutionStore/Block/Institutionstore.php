<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of banner
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Institutionstore extends Mage_Core_Block_Template
{
    
    public function getCityCollection(){
        $collection = Mage::getModel('city/city')->getCollection();
        $collection->addFieldToFilter('city_status', '1');
        $results = $collection->getData();
        if (count($results) > 0) {
            $city_name = array();
            foreach ($results as $value) {
                $city_name[$value['city_id']] = $value['city_name'];
            }
        }
        return $city_name;
    }
    
    public function getFormAction(){
        return $this->getUrl('institutionstore/index/search');
    }
    
    public function getStoreCollection(){
        $StoreCollection = Mage::getModel("institutionstore/institutionstore")->getCollection()->getData();
        $storeArray = array();
        if(count($StoreCollection) > 0){
            foreach($StoreCollection as $keys=>$values){
                $storeArray[] = array(
                        'url_key'=>$values['url_key'],
                        'name'=>$values['name'],
                );
            }
        }
        return $storeArray;
    }
}