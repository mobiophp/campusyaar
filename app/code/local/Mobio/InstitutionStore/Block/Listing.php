<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Store
 *
 * @author root
 */
class Mobio_InstitutionStore_Block_Listing extends Mage_Catalog_Block_Product_List {

    protected $_productCollection;

    protected function _getProductCollection() {
        $store_name = $this->getStoreKey();
        $storeProduct = $this->getStoreIndentity($store_name);
        if (is_null($this->_productCollection)) {

            $params = $this->getRequest()->getParams();

            $ProductArray = $storeProduct;
            $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
            $collection = Mage::getModel('catalog/product')
                    ->getCollection()
                    ->addAttributeToFilter('entity_id', array('in' => explode(",", $ProductArray)))
                    ->addAttributeToSelect($attributes);

            if (isset($params['tshirt_type'])) {
                $collection->addAttributeToFilter('tshirt_type', $params['tshirt_type']);
            }
            Mage::getModel('catalog/layer')->prepareProductCollection($collection);
            $this->_productCollection = $collection;
        }
        return parent::_getProductCollection();
    }

    public function getStoreIndentity($storeIndentity) {
        try {

            if ($storeIndentity != "") {
                $Storemodel = Mage::getModel("institutionstore/institutionstore")
                        ->getCollection()
                        ->addFieldToFilter('url_key', "$storeIndentity")
                        ->getData();
                if (count($Storemodel) > 0) {
                    $product = $Storemodel[0]['product'];
                    return $product;
                }
            }
        } catch (Exception $ex) {
            Mage::getSingleton("core/session")->addError($this->__("Something Wrong ! Store Not Found") . ' ' . $ex->getMessage());
        }
    }

    public function getStoreData() {
        $StoreKey = $this->getStoreKey();
        $Storemodel = Mage::getModel("institutionstore/institutionstore")
                ->getCollection()
                ->addFieldToFilter('url_key', "$StoreKey")
                ->getData();
        if (count($Storemodel) > 0) {
            return $Storemodel[0];
        }
    }

    protected function getStoreKey() {
        $pathInfo = $this->getRequest()->getPathInfo();
        $param = explode("/", $pathInfo);
        return $param[2];
    }

    public function toOptionArray(){
        
        //$ignoreAttributes = array('sku', 'name', 'attribute_set_id', 'type_id', 'qty', 'price', 'status', 'visibility');
        $ignoreAttributes = array('tshirt_type','gender');
        $collection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addVisibleFilter();
        $result = array();
        foreach ($collection as $model) {
            if (!in_array($model->getAttributeCode(), $ignoreAttributes)) {
                continue;
            }
            $productCollection = $this->_productCollection;
            $productCollection->addAttributeToSelect(array($model->getAttributeCode()));
            $productCollection->addAttributeToFilter($model->getAttributeCode(), array('gt' => 0));
            if(count($productCollection->getData()) > 0) {
                $result[$model->getAttributeCode()] = array('value' => $model->getAttributeCode(), 'label' => $model->getFrontendLabel());
            }
        }
        
        if(count($result) > 0){
            foreach($result as $keys=>$values){
                $attributeCode = $values['value'];
                $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$attributeCode);
                if($attribute->usesSource()){
                    $options = $attribute->getSource()->getAllOptions(false);
                    $result[$attributeCode] = $options;
                }
            }
        }
        return $result;
    }
}