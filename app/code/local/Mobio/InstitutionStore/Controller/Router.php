<?php

class Mobio_InstitutionStore_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract {

    public function initControllerRouters($observer) {

        $front = $observer->getEvent()->getFront();
        $front->addRouter('store', $this);
        return $this;
    }

    public function match(Zend_Controller_Request_Http $request) {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect(Mage::getUrl('install'))
                    ->sendResponse();
            exit;
        }

        $requestParam = $request->getParams();
        $pathInfo = trim($request->getPathInfo(), '/');
        $params = explode('/', $pathInfo);
        if (isset($params[0]) && $params[0] == 'store' && !isset($requestParam['layer_action'])) {
            $request->setModuleName('institutionstore')
                    ->setControllerName('index')
                    ->setActionName('list');
            if (isset($params[4])) {
                $request->setParam('store_identity', $params[4]);
            }
            $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $redirectURl
            );
            return true;
        }elseif (isset($params[0]) && $params[0] == 'store' && isset($requestParam['layer_action'])){
            $request->setModuleName('institutionstore')
                    ->setControllerName('index')
                    ->setActionName('list');
            if (isset($params[4])) {
                $request->setParam('store_identity', $params[4]);
            }
            $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $redirectURl
            ); 
            return true;
        }
        else {

            $request->setModuleName('institutionstore')
                    ->setControllerName('index')
                    ->setActionName('listajax');
            $request->setParam($request->getParams());
        }
        return false;
    }

}
