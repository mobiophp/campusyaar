<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BannerController
 *
 * @author root
 */
class Mobio_InstitutionStore_Adminhtml_InstitutionstoreController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward("edit");
    }

    public function editAction() {

        $banner_id = $this->getRequest()->getParam("id");
        Mage::register("institutionstore", Mage::getModel("institutionstore/institutionstore")->load($banner_id));
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('institutionstore/adminhtml_institutionstore_edit'))
                ->_addLeft($this->getLayout()->createBlock('institutionstore/adminhtml_institutionstore_edit_tabs'));
        $this->renderLayout();
    }

    public function producttabAction() {

        $this->loadLayout();
        $this->getLayout()->getBlock('institutionstore.tab.product')
                ->setProducts($this->getRequest()->getPost('products', null));
        $this->renderLayout();
    }

    public function productGridAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('institutionstore.tab.product')
                ->setProducts($this->getRequest()->getPost('products', null));
        $this->renderLayout();
    }

    public function saveAction() {


        try {

            $data = $this->getRequest()->getPost();

            $urlKeyName = $data['institution_name'];
            $urlKey = strtolower(preg_replace('#[^0-9a-z]+#i', '-', $urlKeyName));
            if (key_exists("id", $data)) {
                $store = Mage::getModel("institutionstore/institutionstore");
                $store = $store->load($data["id"]);

                $storeCollection = Mage::getModel("institutionstore/institutionstore")
                        ->getCollection()
                        ->addFieldToFilter("url_key", $urlKey)
                        ->getSize();

                if ($storeCollection > 1) {
                    $urlKey = $urlKey . ' ' . $data["code"];
                    $urlKey = strtolower(preg_replace('#[^0-9a-z]+#i', '-', $urlKey));
                }
            } else {

                $storeCollection = Mage::getModel("institutionstore/institutionstore")
                        ->getCollection()
                        ->getSize();
                if ($storeCollection > 0) {
                    $storeCollection = Mage::getModel("institutionstore/institutionstore")
                            ->getCollection()
                            ->addFieldToFilter("url_key", $urlKey)
                            ->getSize();
                    if ($storeCollection > 0) {
                        $urlKey = $urlKey . ' ' . $data["code"];
                        $urlKey = strtolower(preg_replace('#[^0-9a-z]+#i', '-', $urlKeyName));
                    }
                }

                $store = Mage::getModel("institutionstore/institutionstore");
            }

            if (isset($data['products'])) {
                $dataArray = array();
                $productIds = "";
                $products = Mage::helper('adminhtml/js')->decodeGridSerializedInput($data['products']['product_id']);
                if (count($products)) {
                    foreach ($products as $keys => $values) {
                        $dataArray[] = $keys;
                    }
                    $productIds = implode(",", $dataArray);
                }
                $store->setProduct($productIds);
            }

            $store->setUrlKey($urlKey);
            $store->setType($data["institution_type"]);
            $store->setName($data["institution_name"]);
            $store->setCode($data["code"]);
            $store->setDescription($data["description"]);
            $store->setCountry($data["country"]);
            $store->setState($data["state"]);
            $store->setCity($data["city"]);
            $store->setStatus($data["institution_status"]);


            if (isset($_FILES['logo']['name']) || isset($_FILES['image']['name'])) {
                try {
                    $pathLogo = Mage::getBaseDir("media") . DS . "CY_Store" . DS;


                    $allowedExts = array("gif", "jpeg", "jpg", "png");

                    if ($_FILES['logo']['name'] != '') {
                        $logo = new Varien_File_Uploader('logo');
                        $logo->setAllowedExtensions($allowedExts);
                        $logo->setAllowRenameFiles(true);
                        $logo->setFilesDispersion(false);
                        if (!is_dir($pathLogo)) {
                            mkdir($pathLogo, 0777, true);
                        }

                        $temp = explode(".", $_FILES["logo"]["name"]);
                        $newfilename = round(microtime(true)) . '_' . $temp[0] . '.' . end($temp);
                        $logo->save($pathLogo, $newfilename);
                        $filename = $logo->getUploadedFileName();
                        $store->setLogo($filename);
                    }
                    if ($_FILES['image']['name'] != '') {
                        $banner = new Varien_File_Uploader('image');
                        $banner->setAllowedExtensions(array("jpg", "jpeg", "gif", "png"));
                        $banner->setAllowRenameFiles(true);
                        $banner->setFilesDispersion(FALSE);
                        if (!is_dir($pathLogo)) {
                            mkdir($pathLogo, 0777, true);
                        }
                        $temp = explode(".", $_FILES["image"]["name"]);
                        $newfilename = round(microtime(true)) . '_' . $temp[0] . '.' . end($temp);
                        $banner->save($pathLogo, $newfilename);
                        $bannerfilename = $banner->getUploadedFileName();
                        $store->setImage($bannerfilename);
                    }
                } catch (Exception $e) {
                    Mage::getSingleton("core/session")->addError($this->__("Image could not be uploaded , please try to save again :") . $e->getMessage());
                }
            }

            try {
                $store->save();
                Mage::getSingleton("core/session")->addSuccess($this->__("Institution saved successfully."));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $store->getId()));
                    return;
                }
                $this->_redirect("*/*/index");
            } catch (Exception $ex) {
                Mage::getSingleton("core/session")->addSuccess($this->__($ex->getMessage()));
                $this->_redirect("*/*/index");
            }
        } catch (Exception $e) {
            Mage::getSingleton("core/session")->addError($this->__("Institution could not be saved , please try again :") . $e->getMessage());
            $this->_redirect("*/*/*");
        }
    }

    public function massDeleteAction() {
        $storeIds = $this->getRequest()->getParam('institutionstore');
        if (!is_array($storeIds)) {
            $this->_getSession()->addError($this->__('Please select store(s).'));
        } else {
            if (!empty($storeIds)) {
                try {
                    foreach ($storeIds as $storeId){
                        $institutionstore = Mage::getSingleton('institutionstore/institutionstore')->load($storeId);
                        Mage::dispatchEvent('institutionstore_controller_institutionstore_delete', array('institutionstore' => $institutionstore));
                        $institutionstore->delete();
                    }
                    $this->_getSession()->addSuccess(
                            $this->__('Total of %d record(s) have been deleted.', count($storeIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Update product(s) status action
     *
     */
    public function massStatusAction() {
        $storeIds = (array) $this->getRequest()->getParam('institutionstore');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $status = (int) $this->getRequest()->getParam('status');

        try {
            foreach ($storeIds as $storeId) {
                $institutionstore = Mage::getSingleton('institutionstore/institutionstore')->load($storeId);
                Mage::dispatchEvent('institutionstore_controller_institutionstore_status_change', array('institutionstore' => $institutionstore));
                $institutionstore->setStatus($status);
                $institutionstore->save();
            }
            $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been updated.', count($storeIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                    ->addException($e, $this->__('An error occurred while updating the store(s) status.'));
        }

        $this->_redirect('*/*/', array('store' => $storeId));
    }
}