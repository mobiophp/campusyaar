<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author root
 */
class Mobio_InstitutionStore_IndexController extends Mage_Core_Controller_Front_Action {

    //put your code here
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function listAction() {

        $layer_action = $this->getRequest()->getParam('layer_action');
        if (Mage::helper('layerednavigationajax/data')->isAjax() && $layer_action == 1) {

            $this->loadLayout();
            $this->_initLayoutMessages('checkout/session');
            $this->_initLayoutMessages('tag/session');
            $this->renderLayout();
            $productlist = $this->getLayout()->getBlock('store_list')->toHtml();
            $data['status'] = 1;
            $data['tagtoolbarjs'] = Mage::helper('layerednavigationajax/data')->getToolbarForTagProductJs();
            $data['productlist'] = $productlist;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
            return;
        } else {
            $pathInfo = $this->getRequest()->getPathInfo();
            $param = explode("/", $pathInfo);
            $StoreKey = $param[2];
            $Storemodel = Mage::getModel("institutionstore/institutionstore")
                    ->getCollection()
                    ->addFieldToFilter('url_key', "$StoreKey")
                    ->getData();
            if (count($Storemodel) > 0) {
                $this->loadLayout();
                $this->renderLayout();
            } else {
                $this->_forward("no-routes");
            }
        }
    }

    public function listajaxAction() {

        $layer_action = $this->getRequest()->getParam('layer_action');
        if ($layer_action == 1) {
            $data = array();
            $tagId = $this->getRequest()->getParam('tagId');
            $tag = Mage::getModel('tag/tag')->load($tagId);
            if (!$tag->getId() || !$tag->isAvailableInStore()) {
                $this->_forward('404');
                return;
            }
            Mage::register('current_tag', $tag);

            $this->loadLayout();
            $this->_initLayoutMessages('checkout/session');
            $this->_initLayoutMessages('tag/session');
            $this->renderLayout();

            $layerLeft = $this->getLayout()->getBlock('tags_popular')->toHtml();
            $productlist = $this->getLayout()->getBlock('tag_products')->toHtml();
            $data['status'] = 1;
            $data['leftLayer'] = $layerLeft;
            $data['tagtoolbarjs'] = Mage::helper('layerednavigationajax/data')->getToolbarForTagProductJs();
            $data['productlist'] = $productlist;
            //Mage::log($data, null, 'catalogsearch.log');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
            return;
        }
    }

    public function searchStoreAction() {
        try {
            $response = array();
            $data = $this->getRequest()->getPost();
            //$storetype = $data['storetype'];
            $city = $data['city'];
            if ($city != "") {
                $collection = Mage::getModel("institutionstore/institutionstore")
                        ->getCollection()
                        ->addFieldToFilter('city', $city)
                        //->addFieldToFilter('type', $storetype)
                        ->addFieldToFilter('status', 1)
                        ->addFieldToSelect(array('type', 'code', 'id', 'name', 'url_key'))
                        ->getData();

                $options = "";
                $data = 0;
                if (count($collection) > 0) {
                    foreach ($collection as $keys => $values) {
                        $options .= "<option value=" . $values['url_key'] . ">" . $values['name'] . "</option>";
                    }
                    $data = 1;
                } else {
                    $options .= "<option value=''>Select Store</option>";
                }

                $response['status'] = 'SUCCESS';
                $response['options'] = $options;
                $response['count'] = $data;
            } else {
                $response['status'] = 'ERROR';
                $response['options'] = $this->__("Something Wrong Here!");
                $response['count'] = 0;
            }
        } catch (Exception $ex) {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__($ex->getMessage());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

    public function searchAction() {
        try {

            $postData = $this->getRequest()->getParams();
            if ($postData['store_key'] != "") {
                $storeId = $postData['store_key'];
            } else {
                $storeId = $postData['institute'];
            }
            $collection = Mage::getModel("institutionstore/institutionstore")
                    ->getCollection()
                    ->addFieldToFilter('url_key', $storeId)
                    ->getData();
            $url = $collection[0]['url_key'];
            $redirectUrl = Mage::getUrl('store/' . $url, array('_secure' => true));
            $this->_redirectUrl($redirectUrl);
        } catch (Exception $ex) {

            Mage::getSingleton("core/session")->addError($this->__("Something wrong! please try again :") . $ex->getMessage());
            $this->_redirect("*/*/index");
        }
    }

    public function StoreSearchAction() {
        try {

            $postData = $this->getRequest()->getPost();
            $name = $postData['store_name'];
            $collection = Mage::getModel("institutionstore/institutionstore")
                    ->getCollection()
                    ->addFieldToFilter('name', array('like' => '%' . $name . '%'))
                    ->addFieldToFilter('status', 1)
                    ->getData();

            $StoreList = "";
            if (count($collection) > 0) {
                $StoreList .= '<ul class="store_filter_list">';
                foreach ($collection as $keys => $values) {
                    $urlKey = $values['url_key'];
                    $StoreList .= "<li val=" . $urlKey . ">" . $values['name'] . "</li>";
                }
                $StoreList .= '</ul>';
            } else {
                $StoreList .= '<ul class="store_filter_list">';
                $StoreList .= "<li attr=''>Record Not Found</li>";
                $StoreList .= '</ul>';
            }

            $response['status'] = 1;
            $response['storedata'] = $StoreList;
        } catch (Exception $ex) {
            $response['status'] = 0;
            $response['message'] = $this->__($ex->getMessage());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

}
