<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$installer = $this;


$installer->startSetup();


/**
 * Create table 'institutionstore/institutionstore'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('institutionstore/institutionstore'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store Id')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_INTEGER,null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Institution Type')
    ->addColumn('url_key', Varien_Db_Ddl_Table::TYPE_VARCHAR,1000, array(
        'nullable'  => false,
        'default'   => null,
        ), 'Institution Url')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR,1000, array(
        'nullable'  => false,
        'default'   => null,
        ), 'Institution Name')
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR,1000, array(
        'nullable'  => false,
        'default'   => null,
        ), 'Institution Code')
    ->addColumn('product', Varien_Db_Ddl_Table::TYPE_VARCHAR,1000, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Institution Product')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_VARCHAR,1000, array(
        'nullable'  => false,
        'default'   => null,
        ), 'Institution Description')
    ->addColumn('logo', Varien_Db_Ddl_Table::TYPE_VARCHAR,500, array(
        'unsigned'  => false,
        'nullable'  => false,
        ), 'Institution Logo')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR,500, array(
        'unsigned'  => false,
        'nullable'  => false,
        ), 'Institution Image')
    ->addColumn('city', Varien_Db_Ddl_Table::TYPE_INTEGER,null,array(
        'unsigned'  => false,
        'nullable'  => false,
        ), 'Institution City')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Institution Status')
    ->setComment('Institution table store the information about the institution.');
$installer->getConnection()->createTable($table);
$installer->endSetup();
?>