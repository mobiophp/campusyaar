<?php
class Mobio_Quote_Block_Adminhtml_Quote extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_quote';
    $this->_blockGroup = 'quote';
    $this->_headerText = Mage::helper('quote')->__('Quote Manager');
    $this->_addButtonLabel = Mage::helper('quote')->__('Add Quote');
	
    parent::__construct();
    $this->_removeButton('add');
  }
}