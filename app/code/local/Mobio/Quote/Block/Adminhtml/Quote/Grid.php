<?php

class Mobio_Quote_Block_Adminhtml_Quote_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('quoteGrid');
      $this->setDefaultSort('quote_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('quote/quote')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('quote_id', array(
          'header'    => Mage::helper('quote')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'quote_id',
      ));

	
      $this->addColumn('quote_name', array(
          'header'    => Mage::helper('quote')->__('Quote Name'),
          'align'     =>'left',
          'index'     => 'quote_name',
      ));
      
      $this->addColumn('quote_phone_number', array(
          'header'    => Mage::helper('quote')->__('Quote Contact Number'),
          'align'     =>'left',
          'index'     => 'quote_phone_number',
      ));
      $this->addColumn('quote_email_id', array(
          'header'    => Mage::helper('quote')->__('Quote Email Id'),
          'align'     =>'left',
          'index'     => 'quote_email_id',
      ));
	  
      
      $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('quote')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('quote')->__('View'),
                        'url'       => array('base'=> '*/*/view'),
                        'field'     => 'id'
                    ),
                    array(
                        'caption'   => Mage::helper('quote')->__('Delete'),
                        'url'       => array('base'=> '*/*/delete'),
                        'field'     => 'id',
                        'confirm'  => Mage::helper('quote')->__('Are you sure?')
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
      
      
        
      
	//	$this->addExportType('*/*/exportCsv', Mage::helper('banners')->__('CSV'));
	//	$this->addExportType('*/*/exportXml', Mage::helper('banners')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('quote_id');
        $this->getMassactionBlock()->setFormFieldName('quote');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('quote')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('quote')->__('Are you sure?')
        ));

        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/view', array('id' => $row->getId()));
  }

}