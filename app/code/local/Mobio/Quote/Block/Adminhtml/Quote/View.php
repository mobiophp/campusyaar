<?php

class Mobio_Quote_Block_Adminhtml_Quote_View extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function getQuote() {
        $id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('quote/quote')->getCollection();
        $collection->addFieldToFilter('quote_id', $id);
        $extrabrand = $collection->getFirstItem();
        return $extrabrand->getData();
    }

    public function getHeaderText() {
        return Mage::helper('quote')->__('View Quote');
    }

    public function getButtonsHtml() {
        $data = array(
            'label' => 'Back',
            'onclick' => 'setLocation(\'' . $this->getUrl('*/*/*') . '\')',
            'class' => 'back'
        );
        return $this->addButton('Back', $data, 0, 100, 'header');
    }
}