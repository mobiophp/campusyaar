<?php

class Mobio_Quote_Model_Status extends Varien_Object {

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    static public function getOptionArray() {
        return array(
            self::STATUS_ENABLED => Mage::helper('adminhtml')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('adminhtml')->__('Disabled')
        );
    }

    public function getCities() {

        $collection = Mage::getModel('city/city')->getCollection();
        $collection->addFieldToFilter('city_status', '1');
        $results = $collection->getData();
        if (count($results) > 0) {
            $city_name = array();
            foreach ($results as $value) {
                $city_name[$value['city_id']] = $value['city_name'];
            }
        }
        return $city_name;
    }

}
