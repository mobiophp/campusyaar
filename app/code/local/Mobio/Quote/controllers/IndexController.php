<?php

class Mobio_Quote_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
        // echo Mage::getSingleton('core/design_package')->getTheme('frontend');
    }

    public function postAction() {
        $post = $this->getRequest()->getPost();
        $post['created_time'] = date('Y-m-d H:i:s');

        if ($post) {


            try {

                $model = Mage::getModel('quote/quote')->setData($post);
                $insertId = $model->save()->getId();

                $emailTemplate  = Mage::getModel('core/email_template')
						->loadDefault('quote_user_email_template');
                $city_name = Mage::getSingleton('quote/status')->getCities();
                
                $emailTemplateVariables = array();
                $emailTemplateVariables['quote_name'] = $post['quote_name'];
                $emailTemplateVariables['quote_email_id'] = $post['quote_email_id'];
                $emailTemplateVariables['quote_phone_number'] = $post['quote_phone_number'];
                $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
                //$emailTemplate->send($post['quote_email_id'],$post['quote_name'], $emailTemplateVariables);
                
                $admin_email = Mage::getStoreConfig('trans_email/ident_general/email');
                $admin_name = Mage::getStoreConfig('trans_email/ident_general/name');
                
                $mail = Mage::getModel('core/email')
                            ->setToName($post['quote_name'])
                            ->setToEmail($post['quote_email_id'])
                            ->setBody($processedTemplate)
                            ->setSubject('Subject :')
                            ->setFromEmail($admin_email)
                            ->setFromName($admin_name)
                            ->setType('html');
                
                $mail->send();
                
                
                $adminemailTemplate  = Mage::getModel('core/email_template')
						->loadDefault('quote_admin_email_template');
                $adminprocessedTemplate = $adminemailTemplate->getProcessedTemplate($emailTemplateVariables);
                //$adminemailTemplate->send($post['quote_email_id'],$post['quote_name'], $emailTemplateVariables);
                
                $adminmail = Mage::getModel('core/email')
                            ->setToName($admin_name)
                            ->setToEmail($admin_email)
                            ->setBody($adminprocessedTemplate)
                            ->setSubject('Subject :')
                            ->setType('html');
                
                $adminmail->send();

                Mage::getSingleton('core/session')->addSuccess(Mage::helper('quote')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {


                Mage::getSingleton('core/session')->addError(Mage::helper('quote')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

}
