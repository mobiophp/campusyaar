<?php

$installer = $this;

$installer->startSetup();


$installer->run("
DROP TABLE IF EXISTS `quote`;
CREATE TABLE `quote` (                                  
		   `quote_id` int(11) unsigned NOT NULL auto_increment,  
		   `quote_name` varchar(255) NOT NULL default '',               
		   `quote_phone_number` bigint(20) NOT NULL default '0',
                   `quote_email_id` varchar(255) NOT NULL default '',
                   `message` text NOT NULL,
		   `created_time` datetime default NULL,                   
		   PRIMARY KEY  (`quote_id`)                             
		 ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

");
$installer->endSetup(); 

?>