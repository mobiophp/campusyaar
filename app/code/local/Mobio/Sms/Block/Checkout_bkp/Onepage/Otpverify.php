<?php
class Mobio_Sms_Block_Checkout_Onepage_Otpverify extends Mage_Checkout_Block_Onepage_Abstract{
    protected function _construct()
    {
        $this->getCheckout()->setStepData('otpverify', array(
            'label'     => Mage::helper('checkout')->__('Booking Verification'),
            'is_show'   => $this->isShow()
        ));
        if($this->isCustomerLoggedIn()){
            $this->getCheckout()->setStepData('otpverify', 'allow', true);
            $this->getCheckout()->setStepData('billing', 'allow', false);
        }
        
        parent::_construct();
    }
}