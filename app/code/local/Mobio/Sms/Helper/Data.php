<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 *
 * @author root
 */
class Mobio_Sms_Helper_Data extends Mage_Customer_Helper_Data {

    const SMS_GENERAL_OPTION_SMS_STATUS = 'sms/general_option/sms_status';
    const SMS_GENERAL_OPTION_SMS_URL = 'sms/general_option/sms_url';
    const SMS_GENERAL_OPTION_SMS_APIKEY = 'sms/general_option/sms_apikey';
    const SMS_GENERAL_OPTION_SMS_SENDERID = 'sms/general_option/sms_senderid';
    const SMS_GENERAL_OPTION_SMS_ROUTE = 'sms/general_option/sms_route';
    const CONFIG_PATH_OTP_ENABLED = 'sms/otp_setting/otp_status';

    /**
     * Check customer is logged in
     *
     * @return bool
     */
    public function isLoggedIn() {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    public function sendSms($mobileno, $verificationCode) {

        $Configuration = $this->getSmsConfiguration($mobileno, $verificationCode);

        $url = $Configuration['url'];
        $mobileno = $Configuration['postData']['mobiles'];
        $userId = $Configuration['senderId'];
        $authKey = $Configuration['authKey'];
        $msg_type = $Configuration['route'];

        $str = $verificationCode . ' is your one time verification code for Register with Campusyaari. It is valid for 10 mins. Enjoy :)';
        $Message = urlencode($str);
        $CurlUrl = $url . '?method=SendMessage&send_to=' . $mobileno . '&msg=' . $Message . '&msg_type=' . $msg_type . '&userid=' . $userId . '&auth_scheme=plain&password=' . $authKey . '&v=1.1&format=text';
        return $this->sendSMSCurl($CurlUrl);
    }

    public function sendCodSms($mobileno, $verificationCode) {

        $Configuration = $this->getSmsConfiguration($mobileno, $verificationCode);

        $url = $Configuration['url'];
        $mobileno = $Configuration['postData']['mobiles'];
        $userId = $Configuration['senderId'];
        $authKey = $Configuration['authKey'];
        $msg_type = $Configuration['route'];

        $str = $verificationCode . " is your one time verification code for Product Purchase at Campusyaari. Please don't share with anyone.";
        $Message = urlencode($str);
        $CurlUrl = $url . '?method=SendMessage&send_to=' . $mobileno . '&msg=' . $Message . '&msg_type=' . $msg_type . '&userid=' . $userId . '&auth_scheme=plain&password=' . $authKey . '&v=1.1&format=text';
        return $this->sendSMSCurl($CurlUrl);
    }

    protected function sendSMSCurl($CurlUrl) {

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $CurlUrl,
            CURLOPT_RETURNTRANSFER => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);


        if (curl_errno($ch)) {
            return array(
                'status' => 0,
                'message' => curl_error($ch),
            );
        }

        $outPutArray = explode('|', $output);
        if (count($outPutArray) > 0) {
            curl_close($ch);
            $status = $outPutArray[0];
            if (trim($status) == "success") {
                return array(
                    'status' => 1,
                    'message' => 'Success:'
                );
            } else {
                return array(
                    'status' => 0,
                    'message' => 'Something Wrong! OTP Not Sent to your mobile no.'
                );
            }
        } else {
            return array(
                'status' => 0,
                'message' => curl_error($ch),
            );
        }
    }

    public function getSmsConfiguration($mobileno, $message) {

        $authKey = Mage::getStoreConfig(self::SMS_GENERAL_OPTION_SMS_APIKEY);
        $senderId = Mage::getStoreConfig(self::SMS_GENERAL_OPTION_SMS_SENDERID);
        $message = urlencode($message);
        $route = Mage::getStoreConfig(self::SMS_GENERAL_OPTION_SMS_ROUTE);

        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileno,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        return array(
            'authKey' => $authKey,
            'senderId' => $senderId,
            'message' => $message,
            'route' => $route,
            'postData' => $postData,
            'url' => Mage::getStoreConfig(self::SMS_GENERAL_OPTION_SMS_URL),
        );
    }

    public function getSmsStatus() {
        return Mage::getStoreConfig(self::SMS_GENERAL_OPTION_SMS_STATUS);
    }

    public function getOtpKey() {
        //$string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password = substr($string_shuffled, 1, 6);
        return $password;
    }

    public function getVerifyPostUrl() {
        return $this->_getUrl('customer/account/verifypost');
    }

    public function getBackUrl() {
        return $this->_getUrl('customer/account/create');
    }

    public function isCodOtpVerifcationEnabled() {
        return Mage::getStoreConfigFlag(self::CONFIG_PATH_OTP_ENABLED);
    }
}