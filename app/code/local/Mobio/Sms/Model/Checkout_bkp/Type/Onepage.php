<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * One page checkout processing model
 */
class Mobio_Sms_Model_Checkout_Type_Onepage extends Mage_Checkout_Model_Type_Onepage {

    public function VerifyBooking($data,$quoteData){

        if(empty($data)){
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }

        $mobileNo = $this->getMobileVerificationNo();
        $quoteId = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $veriMobileNo = $quoteData['mobile_no'];
        $veriQuoteId = $quoteData['quoteId'];
        $veriOtpVerfiy = $quoteData['otp_verfiy'];
        $PostCode = $data['verfication_code']; 
        $otpkey = $quoteData['otpkey'];
      
        if($PostCode == $otpkey && $mobileNo == $veriMobileNo && $veriQuoteId == $quoteId && $veriOtpVerfiy == '0'){
            $updateArray = array(
                'otp_verfiy'=>1
            );
            $this->UpdateOtpVerificationData($updateArray);
            
            $this->getCheckout()
                ->setStepData('otpverify', 'complete', true)
                ->setStepData('shipping_method', 'allow', true);
            
        }else{
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid Code Apply'));
        }
    }

    public function UpdateOtpVerificationData($updateArray = array()){
        $verificationData = $this->getOtpVerficationData();
        if(!empty($verificationData)){
            foreach($updateArray as $key=>$values){
                $verificationData[$key] = $values;
            }
        }
        Mage::getSingleton('checkout/session')->setData('OtpVerfication',$verificationData);
    }

        /**
     * Save checkout shipping address
     *
     * @param   array $data
     * @param   int $customerAddressId
     * @return  Mage_Checkout_Model_Type_Onepage
     */
    public function saveShipping($data, $customerAddressId) {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }
        $address = $this->getQuote()->getShippingAddress();

        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
                ->setEntityType('customer_address')
                ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        if (!empty($customerAddressId)) {
            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
            if ($customerAddress->getId()) {
                if ($customerAddress->getCustomerId() != $this->getQuote()->getCustomerId()) {
                    return array('error' => 1,
                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
                    );
                }

                $address->importCustomerAddress($customerAddress)->setSaveInAddressBook(0);
                $addressForm->setEntity($address);
                $addressErrors = $addressForm->validateData($address->getData());
                if ($addressErrors !== true) {
                    return array('error' => 1, 'message' => $addressErrors);
                }
            }
        } else {
            $addressForm->setEntity($address);
            // emulate request object
            $addressData = $addressForm->extractData($addressForm->prepareRequest($data));
            $addressErrors = $addressForm->validateData($addressData);
            if ($addressErrors !== true) {
                return array('error' => 1, 'message' => $addressErrors);
            }
            $addressForm->compactData($addressData);
            // unset shipping address attributes which were not shown in form
            foreach ($addressForm->getAttributes() as $attribute) {
                if (!isset($data[$attribute->getAttributeCode()])) {
                    $address->setData($attribute->getAttributeCode(), NULL);
                }
            }

            $address->setCustomerAddressId(null);
            // Additional form data, not fetched by extractData (as it fetches only attributes)
            $address->setSaveInAddressBook(empty($data['save_in_address_book']) ? 0 : 1);
            $address->setSameAsBilling(empty($data['same_as_billing']) ? 0 : 1);
        }

        $address->implodeStreetAddress();
        $address->setCollectShippingRates(true);

        if (($validateRes = $address->validate()) !== true) {
            return array('error' => 1, 'message' => $validateRes);
        }

        $this->getQuote()->collectTotals()->save();

        $this->getCheckout()
                ->setStepData('shipping', 'complete', true)
                ->setStepData('shipping_method', 'allow', true);

        return array();
    }

    /**
     * Save billing address information to quote
     * This method is called by One Page Checkout JS (AJAX) while saving the billing information.
     *
     * @param   array $data
     * @param   int $customerAddressId
     * @return  Mage_Checkout_Model_Type_Onepage
     */
    public function saveBilling($data, $customerAddressId) {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }

        $address = $this->getQuote()->getBillingAddress();
        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
                ->setEntityType('customer_address')
                ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        if (!empty($customerAddressId)) {
            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
            if ($customerAddress->getId()) {
                if ($customerAddress->getCustomerId() != $this->getQuote()->getCustomerId()) {
                    return array('error' => 1,
                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
                    );
                }

                $address->importCustomerAddress($customerAddress)->setSaveInAddressBook(0);
                $addressForm->setEntity($address);
                $addressErrors = $addressForm->validateData($address->getData());
                if ($addressErrors !== true) {
                    return array('error' => 1, 'message' => $addressErrors);
                }
            }
        } else {
            $addressForm->setEntity($address);
            // emulate request object
            $addressData = $addressForm->extractData($addressForm->prepareRequest($data));
            $addressErrors = $addressForm->validateData($addressData);
            if ($addressErrors !== true) {
                return array('error' => 1, 'message' => array_values($addressErrors));
            }
            $addressForm->compactData($addressData);
            //unset billing address attributes which were not shown in form
            foreach ($addressForm->getAttributes() as $attribute) {
                if (!isset($data[$attribute->getAttributeCode()])) {
                    $address->setData($attribute->getAttributeCode(), NULL);
                }
            }
            $address->setCustomerAddressId(null);
            // Additional form data, not fetched by extractData (as it fetches only attributes)
            $address->setSaveInAddressBook(empty($data['save_in_address_book']) ? 0 : 1);
        }

        // set email for newly created user
        if (!$address->getEmail() && $this->getQuote()->getCustomerEmail()) {
            $address->setEmail($this->getQuote()->getCustomerEmail());
        }

        // validate billing address
        if (($validateRes = $address->validate()) !== true) {
            return array('error' => 1, 'message' => $validateRes);
        }

        $address->implodeStreetAddress();

        if (true !== ($result = $this->_validateCustomerData($data))) {
            return $result;
        }

        if (!$this->getQuote()->getCustomerId() && self::METHOD_REGISTER == $this->getQuote()->getCheckoutMethod()) {
            if ($this->_customerEmailExists($address->getEmail(), Mage::app()->getWebsite()->getId())) {
                return array('error' => 1, 'message' => $this->_customerEmailExistsMessage);
            }
        }

        if (!$this->getQuote()->isVirtual()) {
            /**
             * Billing address using otions
             */
            $usingCase = isset($data['use_for_shipping']) ? (int) $data['use_for_shipping'] : 0;

            switch ($usingCase) {
                case 0:
                    $shipping = $this->getQuote()->getShippingAddress();
                    $shipping->setSameAsBilling(0);
                    break;
                case 1:
                    $billing = clone $address;
                    $billing->unsAddressId()->unsAddressType();
                    $shipping = $this->getQuote()->getShippingAddress();
                    $shippingMethod = $shipping->getShippingMethod();

                    // Billing address properties that must be always copied to shipping address
                    $requiredBillingAttributes = array('customer_address_id');

                    // don't reset original shipping data, if it was not changed by customer
                    foreach ($shipping->getData() as $shippingKey => $shippingValue) {
                        if (!is_null($shippingValue) && !is_null($billing->getData($shippingKey)) && !isset($data[$shippingKey]) && !in_array($shippingKey, $requiredBillingAttributes)
                        ) {
                            $billing->unsetData($shippingKey);
                        }
                    }
                    $shipping->addData($billing->getData())
                            ->setSameAsBilling(1)
                            ->setSaveInAddressBook(0)
                            ->setShippingMethod($shippingMethod)
                            ->setCollectShippingRates(true);
                    $this->getCheckout()->setStepData('shipping', 'complete', true);
                    break;
            }
        }

        $this->getQuote()->collectTotals();
        $this->getQuote()->save();

        if (!$this->getQuote()->isVirtual() && $this->getCheckout()->getStepData('shipping', 'complete') == true) {
            //Recollect Shipping rates for shipping methods
            $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        }

        $this->getCheckout()
                ->setStepData('billing', 'allow', true)
                ->setStepData('billing', 'complete', true)
                ->setStepData('shipping', 'allow', true);

        return array();
    }
    
    public function getMobileVerificationNo() {

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {

            $CustomerData = array();
            $mobileNo = "";

            $UserId = Mage::getSingleton('customer/session')->getCustomerId();
            $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setCodeFilter('mobile_number')
                    ->getFirstItem();
            $attributeData = $attributeInfo->getData();
            $attributeId = $attributeData['attribute_id'];

            $read = Mage::getModel('core/resource')->getConnection('core_read');
            $select = $read->select()->from(array('cev' => 'customer_entity_varchar'))
                    ->where('cev.attribute_id=?', $attributeId)
                    ->where('cev.entity_id=?', $UserId);
            $CustomerData = $read->fetchAll($select);

            if (count($CustomerData) > 0) {
                $mobileNo = $CustomerData['value'];
            } else {
                $mobileNo = $this->getMobileNoFromQuote();
            }
        } else {
            $mobileNo = $this->getMobileNoFromQuote();
        }

        return $mobileNo;
    }

    public function getMobileNoFromQuote() {
        return Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getTelephone();
    }

    public function setOtpVerficationData($data = array()) {
        Mage::getSingleton('checkout/session')->setData('OtpVerfication',$data);
    }

    public function getOtpVerficationData(){
        return Mage::getSingleton('checkout/session')->getData('OtpVerfication');
    }

    public function checkVerificationData(){
        $verificationData = $this->getOtpVerficationData();
        if(!empty($verificationData)){
            $mobileNo = $this->getMobileVerificationNo();
            $quoteId = Mage::getSingleton('checkout/session')->getQuote()->getId();
            $veriMobileNo = $verificationData['mobile_no'];
            $veriQuoteId = $verificationData['quoteId'];
            $optSend = $verificationData['opt_send'];
            $otpVerfiy = $verificationData['otp_verfiy'];
            $mobileNoChange = $verificationData['mobile_no_change'];
           
            if($mobileNo != $veriMobileNo && $quoteId == $veriQuoteId){
                $updateArray = array(
                    'otp_verfiy'=>0,
                    'mobile_no'=>$mobileNo,
                    'opt_send'=>1,
                );
                $this->UpdateOtpVerificationData($updateArray);
                return array(
                        'status'=>1,
                        'mobile_no'=>$mobileNo
                );
            }else{
                return array(
                    'status'=>0
                );
            }
        }
    }
    
    public function IsOtpVerifyed(){
        $quoteData = $this->getOnepage()->getOtpVerficationData();
        if(!empty($quoteData)){
            return $quoteData['otp_verfiy'];
        }else{
            return 0;
        }
    }
}