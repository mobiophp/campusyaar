<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Customer account controller
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once(Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php');

class Mobio_Sms_AccountController extends Mage_Customer_AccountController {

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     * 
     */
    public function preDispatch() {
        // a brute-force protection here would be nice

        parent::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = strtolower($this->getRequest()->getActionName());

        if (preg_match('/^(verify|verifypost)/i', $action)) {
            $flag = 'no-dispatch';
            if ($this->getFlag($action, $flag)) {
                unset($this->_flags[$action][$flag]); // Remove the flag to unset it
                $this->getResponse()->clearHeader('Location'); // Remove Location header for redirect
                $this->getResponse()->setHttpResponseCode(200); // Set HTTP Response Code to OK
            }
        }

        $openActions = array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'changeforgotten',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation',
            'verify'
        );

        $pattern = '/^(' . implode('|', $openActions) . ')/i';
        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }

    /**
     * Create customer account action
     */
    public function createPostAction() {


        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
        if (!$this->_validateFormKey()) {
            $this->_redirectError($errUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($errUrl);
            return;
        }

        $customer = $this->_getCustomer();

        try {
            $errors = $this->_getCustomerErrors($customer);
            if (empty($errors)) {

                /* Start Work of Customer Otp Verficatin code */
                if (Mage::helper("customer")->getSmsStatus()) {

                    $customerArray = $customer->getData();  // Submitted form data
                    //unset($customerArray['password_hash']);
                    $customerData = $customerArray;
                    $otpKey = Mage::helper("customer")->getOtpKey();
                    $customerData['verfication_code'] = $otpKey;
                    $vericationCode = $otpKey;
                    $mobile_number = $customerData['mobile_number'];
                    $Status = Mage::helper("customer")->sendSms($mobile_number, $vericationCode);
                    if ($Status['status']) {
                        $session->setCustomerFormData($customerData);
                        $this->_redirect("*/*/verify");
                        return;
                    } else {
                        $message = $this->__($Status['message']);
                        Mage::getSingleton('core/session')->addError($message);
                        $this->_redirectError($errUrl);
                    }
                } else {
                    $customer->cleanPasswordsValidationData();
                    $customer->save();
                    $this->_dispatchRegisterSuccess($customer);
                    $this->_successProcessRegistration($customer);
                    return;
                }
            } else {
                $this->_addSessionError($errors);
            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
            } else {
                $message = $this->_escapeHtml($e->getMessage());
            }
            $session->addError($message);
        } catch (Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            $session->addException($e, $this->__('Cannot save the customer.'));
        }

        $this->_redirectError($errUrl);
    }

    public function verifyAction() {

        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        try {
            $customerData = $session->getCustomerFormData();
            $VerificationCode = $customerData['verfication_code'];
            if (!empty($VerificationCode)) {
                $this->loadLayout();
                $this->renderLayout();
            } else {
                Mage::getSingleton('core/session')->getMessages(true);
                $session->setCustomerFormData($customerData);
                $message = $this->__('Something Wrong ! please try again');
                $session->addError($message);
                $this->_redirectError($errUrl);
            }
        } catch (Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            $session->addException($e, $this->__('Cannot save the customer.'));
            $this->_redirectError($errUrl);
        }
    }

    public function verifypostAction() {


        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
        $verifyUrl = $this->_getUrl('*/*/verify', array('_secure' => true));
        if (!$this->_validateFormKey()) {
            $this->_redirectError($errUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($errUrl);
            return;
        }

        $postData = $this->getRequest()->getPost();
        $customerData = $session->getCustomerFormData();
        $verificationCode = $postData['verfication_code'];
        $sessionVerificationCode = $customerData['verfication_code'];
        $customer = $this->_getCustomer();
        try {
            if (!empty($sessionVerificationCode)) {
                if ($verificationCode == $sessionVerificationCode) {
                    //$getData = $customer->getData();
                    //$CustomerPostData = array_merge($getData, $this->getRequest()->getPost());
                    $customerData['otp_verfication'] = 1;
                    $customer->setData($customerData);
                    $customer->cleanPasswordsValidationData();
                    $customer->save();
                    $this->_dispatchRegisterSuccess($customer);
                    $this->_successProcessRegistration($customer);
                    return;
                } else {
                    $message = $this->__('Invalid OTP Code Apply');
                    Mage::getSingleton('core/session')->addError($message);
                    $this->_redirectError($verifyUrl);
                }
            } else {
                $this->_redirect('*/*/');
                return;
            }
        } catch (Exception $ex) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            $session->addError($ex->getMessage());
            $this->_redirectError($errUrl);
        }
    }

    /**
     * Success Registration
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_AccountController
     */
    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer) {
        $session = $this->_getSession();
        if ($customer->isConfirmationRequired()) {
            /** @var $app Mage_Core_Model_App */
            $app = $this->_getApp();
            /** @var $store  Mage_Core_Model_Store */
            $store = $app->getStore();
            $customer->sendNewAccountEmail(
                    'confirmation', $session->getBeforeAuthUrl(), $store->getId()
            );
            $customerHelper = $this->_getHelper('customer');
            $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', $customerHelper->getEmailConfirmationUrl($customer->getEmail())));
            $url = $this->_getUrl('*/*/index', array('_secure' => true));
        } else {
            $session->setCustomerAsLoggedIn($customer);
            $this->UpdateAddressData($customer);
            $url = $this->_welcomeCustomer($customer);
        }
        $this->_redirectSuccess($url);
        return $this;
    }

    protected function UpdateAddressData($customer) {

        $address = Mage::getModel("customer/address");
        $address->setCustomerId($customer->getId())
                ->setFirstname($customer->getFirstname())
                ->setMiddleName($customer->getMiddlename())
                ->setLastname($customer->getLastname())
                ->setCountryId('')
                ->setPostcode('')
                ->setCity('')
                ->setTelephone($customer->getMobileNumber())
                ->setFax('')
                ->setCompany('')
                ->setStreet('')
                ->setIsDefaultBilling('0')
                ->setIsDefaultShipping('0')
                ->setSaveInAddressBook('0');
        $address->save();
    }

}
