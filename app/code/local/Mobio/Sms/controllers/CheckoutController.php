<?php

class Mobio_Sms_CheckoutController extends Mage_Core_Controller_Front_Action {

    protected $order;

    protected function _expireAjax() {
        if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) {
            $this->getResponse()->setHeader('HTTP/1.1', '403 Session Expired');
            exit;
        }
    }

    public function orderAction() {

        // Update Quote of Customer
        $this->savePayment();
        $bA = $this->_getQuote()->getBillingAddress();

        $otpKey = Mage::helper("customer")->getOtpKey();
        $vericationCode = $otpKey;
        $mobile_number = $bA->getTelephone();
        $Status = Mage::helper("customer")->sendCodSms($mobile_number, $vericationCode);

        if ($Status['status'] == 1) {
            $this->_getCheckoutSession()->setOrderOtpVerification($vericationCode);
        }

        $responseArray['customer_phone'] = $bA->getTelephone() ? : '';

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->setHeader('Content-type', 'application/json', true)
                ->setBody(json_encode($responseArray));

        return $this;
    }

    /**
     * Return checkout quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote() {
        if (!$this->_quote) {
            $this->_quote = Mage::getSingleton('checkout/session')->getQuote();
        }

        return $this->_quote;
    }

    /**
     * Returns checkout model instance, native onepage checkout is used
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    protected function _getCheckout() {
        return Mage::getSingleton('checkout/type_onepage');
    }

    protected function _getCheckoutSession() {
        return Mage::getSingleton('checkout/session');
    }

    protected function savePayment() {

        $data['method'] = 'msp_cashondelivery';
        $quote = $this->_getQuote();
        if ($quote->isVirtual()) {
            $quote->getBillingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        } else {
            $quote->getShippingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        }

        // shipping totals may be affected by payment method
        if (!$quote->isVirtual() && $quote->getShippingAddress()) {
            $quote->getShippingAddress()->setCollectShippingRates(true);
        }

        $data['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;

        $payment = $quote->getPayment();
        $payment->importData($data);
        $quote->save();
    }

    public function otpverificationAction() {

        $postData = $this->getRequest()->getPost();
        $OtpCode = $postData['otp_code'];

        $vericationCode = $this->_getCheckoutSession()->getOrderOtpVerification();
        $responseArray = array();
        if ($OtpCode == $vericationCode) {
            $responseArray = array(
                'stauts' => 1,
                'msg' => 'OTP Code is match'
            );
            $this->_getCheckoutSession()->unsOrderOtpVerification();
        } else {
            $responseArray = array(
                'stauts' => 0,
                'msg' => 'OTP Code Invalid'
            );
        }

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->setHeader('Content-type', 'application/json', true)
                ->setBody(json_encode($responseArray));

        return $this;
    }

    public function otpresetAction() {

        $bA = $this->_getQuote()->getBillingAddress();
        $otpKey = Mage::helper("customer")->getOtpKey();
        $mobile_number = $bA->getTelephone();
        $Status = Mage::helper("customer")->sendCodSms(trim($mobile_number), $otpKey);
        if ($Status['status'] == 1) {
            $this->_getCheckoutSession()->setOrderOtpVerification($otpKey);
            $vericationCode = $this->_getCheckoutSession()->getOrderOtpVerification();
            $responseArray = array(
                'stauts' => 1,
                'msg' => 'OTP Code Sent on your billing mobile number'
            );
        } else {
            $responseArray = array(
                'stauts' => 0,
                'msg' => 'Somthing Wrong. Please check your billing number'
            );
        }


        $this->getResponse()
                ->setHttpResponseCode(200)
                ->setHeader('Content-type', 'application/json', true)
                ->setBody(json_encode($responseArray));

        return $this;
    }

}
