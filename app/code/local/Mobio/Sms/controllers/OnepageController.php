<?php

require_once(Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'OnepageController.php');

class Mobio_Sms_OnepageController extends Mage_Checkout_OnepageController {
    /**
     * Save checkout billing address
     */

    /**
     * Save checkout billing address
     */
    public function saveBillingAction() {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Shipping address save action
     */
    public function saveShippingAction() {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);

            if (!isset($result['error'])) {
                $result['goto_section'] = 'payment';
                $result['update_section'] = array(
                    'name' => 'payment-method',
                    'html' => $this->_getPaymentMethodsHtml()
                );
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Shipping method save action
     */
    public function saveShippingMethodAction() {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping_method', '');
            $result = $this->getOnepage()->saveShippingMethod($data);
            // $result will contain error data if shipping method is empty
            if (!$result) {
                Mage::dispatchEvent(
                        'checkout_controller_onepage_save_shipping_method', array(
                    'request' => $this->getRequest(),
                    'quote' => $this->getOnepage()->getQuote()));
                $this->getOnepage()->getQuote()->collectTotals();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );
            }
            $this->getOnepage()->getQuote()->collectTotals()->save();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Save payment ajax action
     *
     * Sets either redirect or a JSON response
     */
    public function savePaymentAction() {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            if (!$this->getRequest()->isPost()) {
                $this->_ajaxRedirectResponse();
                return;
            }

            $data = $this->getRequest()->getPost('payment', array());
            $result = $this->getOnepage()->savePayment($data);

            // get section and redirect data
            $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if (empty($result['error']) && !$redirectUrl) {

                $paymentMethod = $data['method'];

                if ($paymentMethod == 'razorpay') {
                    $data = 'freeshipping_freeshipping';
                    $result = $this->getOnepage()->saveShippingMethod($data);

                    Mage::dispatchEvent(
                            'checkout_controller_onepage_save_shipping_method', array(
                        'request' => $this->getRequest(),
                        'quote' => $this->getOnepage()->getQuote()));
                    $this->getOnepage()->getQuote()->collectTotals()->save();
                }

                if ($paymentMethod == 'msp_cashondelivery') {
                    $data = 'freeshipping_freeshipping';
                    $result = $this->getOnepage()->saveShippingMethod($data);
//                    $data = 'tablerate_bestway';
//                    $result = $this->getOnepage()->saveShippingMethod($data);
                    Mage::dispatchEvent(
                            'checkout_controller_onepage_save_shipping_method', array(
                        'request' => $this->getRequest(),
                        'quote' => $this->getOnepage()->getQuote()));
                    $this->getOnepage()->getQuote()->collectTotals()->save();
                }

                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );

//                $result['goto_section'] = 'shipping_method';
//                $result['update_section'] = array(
//                    'name' => 'shipping-method',
//                    'html' => $this->_getShippingMethodsHtml()
//                );
            }
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = $this->__('Unable to set Payment Method.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Order success action
     */
    public function successAction() {
        $session = $this->getOnepage()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }

        // echo $lastOrderId;
        $orderData = $session->getData();
        $RealorderId = $orderData['last_real_order_id'];
        $order = Mage::getModel('sales/order')->loadByIncrementId($RealorderId);
        $billingAddress = $order->getBillingAddress();
        $billingData = $billingAddress->getData();
        $mobileNo = $billingData['telephone'];
        $configuration = Mage::helper("customer")->getSmsConfiguration($mobileNo, $RealorderId);
        $this->sendOrderSms($configuration, $mobileNo, $RealorderId);
        $session->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
    }

    protected function sendOrderSms($Configuration, $mobileNo, $RealorderId) {

        $url = $Configuration['url'];
        $userId = $Configuration['senderId'];
        $authKey = $Configuration['authKey'];
        $msg_type = $Configuration['route'];

        $SupportEmail = Mage::getStoreConfig('trans_email/ident_support/email');
        $storePhone = Mage::getStoreConfig('general/store_information/phone');
        $storeName = "Campusyaari";
        $str = 'Hi, Thank you for your order from ' . $storeName . '. Your Order id is ' . $RealorderId . '. In case of any queries, kindly call us on ' . $storePhone . ' or write to us at ' . $SupportEmail . '.';
        $Message = urlencode($str);
        $CurlUrl = $url . '?method=SendMessage&send_to=' . $mobileNo . '&msg=' . $Message . '&msg_type=' . $msg_type . '&userid=' . $userId . '&auth_scheme=plain&password=' . $authKey . '&v=1.1&format=text';

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $CurlUrl,
            CURLOPT_RETURNTRANSFER => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
    }

}
