$jQ = jQuery.noConflict();
$jQ(function () {
//    $jQ('.popup-modal').magnificPopup({
//        type: 'inline',
//        preloader: false,
//        modal: true
//    });

    $jQ('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

    $jQ('button.opt_verification_button').magnificPopup({
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,   
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });


    $jQ(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $jQ.magnificPopup.close();
    });
});