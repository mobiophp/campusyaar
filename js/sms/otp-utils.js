var OtpUtils = Class.create();
OtpUtils.prototype = {
    initialize: function (urls) {
        this.orderUrl = urls.orderUrl;
    },
    createOrder: function (onSuccess, onFailure) {
        var self = this;

        function success(event) {
            self.orderInfo = event.responseJSON;

            if (typeof onSuccess === 'function') {
                onSuccess();
            }
        }

        new Ajax.Request(
                this.orderUrl,
                {
                    method: 'post',
                    onSuccess: success,
                    onFailure: onFailure
                }
        );
    },
    OtpVerification: function (verfiyUrl, onSuccess, onFailure, onUserClose, beforeStart)
    {

        var form = $('otp_varification_form');
        var input = form['otp_code'];
        var otp_code = $F(input);
        this.otpUrl = verfiyUrl;

        if (otp_code != "") {

            $('opt-please-wait').show();

            var OTPVerifyOrderButton = $$('button[type=submit][class~="verify_otp"]')[0];
            OTPVerifyOrderButton.disabled = true;
            var OTPResendOrderButton = $$('button[type=submit][class~="resend_otp"]')[0];
            OTPResendOrderButton.disabled = true;

            function success(event) {
                var orderInfo = event.responseJSON;
                if (orderInfo.stauts == 0) {
                    $('otp_message').update();
                    $('otp_message').insert("<span id='fail'>" + orderInfo.msg + "</span>");
                    $('otp_message').show();
                    $('opt-please-wait').hide();
                    OTPVerifyOrderButton.disabled = false;
                    OTPResendOrderButton.disabled = false;
                } else {
                    if (typeof onSuccess === 'function') {
                        $('otp_message').update();
                        $('otp_message').insert("<span id='fail'>" + orderInfo.msg + "</span>");
                        $('otp_message').show();
                        onSuccess();
                        $('opt-please-wait').hide();
                    }
                }

            }

            new Ajax.Request(
                    this.otpUrl,
                    {
                        method: 'post',
                        parameters: {otp_code: otp_code},
                        onSuccess: success,
                        onFailure: onFailure
                    }
            );
        } else {
            $('otp_message').update();
            $('otp_message').show();
            $('otp_message').insert("<span id='fail'>Please enter otp code</span>");
        }

    },
    OtpResend: function (ResendUrl)
    {
        $('opt-please-wait').show();
        this.otpResend = ResendUrl;
        var OTPVerifyOrderButton = $$('button[type=submit][class~="verify_otp"]')[0];
        OTPVerifyOrderButton.disabled = true;
        var OTPResendOrderButton = $$('button[type=submit][class~="resend_otp"]')[0];
        OTPResendOrderButton.disabled = true;

        function success(event) {
            var orderInfo = event.responseJSON;

            OTPVerifyOrderButton.disabled = false;
            OTPResendOrderButton.disabled = false;
            $('opt-please-wait').hide();

            if (orderInfo.stauts == 0) {
                $('otp_message').update();
                $('otp_message').insert("<span id='fail'>" + orderInfo.msg + "</span>");
                $('otp_message').show();

            } else {
                $('otp_message').update();
                $('otp_message').insert("<span id='sucess'>" + orderInfo.msg + "</span>");
                $('otp_message').show();
            }
        }

        new Ajax.Request(
                this.otpResend,
                {
                    method: 'post',
                    onSuccess: success
                }
        );


    }
};