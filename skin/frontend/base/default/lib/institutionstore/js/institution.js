$jQ = jQuery.noConflict();
jQuery(function () {
    jQuery("li").on("click", ".store_filter_list li", function (event) {
        var storeKey = jQuery(this).attr("val");
        var storeValue = jQuery(this).text();
        if (storeKey != "") {
            jQuery("#store_key").val(storeKey);
            jQuery("#store_name").val(storeValue);
            jQuery("#store_search_container").html("");
            jQuery("#store_search_container").hide();

            if (jQuery("#store_city").hasClass("validation-failed")) {
                jQuery("#store_city").removeClass("validation-failed");
                jQuery("#store_city").removeClass("required-entry");
                jQuery("#store_city").next(".validation-advice").hide();
            } else {
                jQuery("#store_city").removeClass("required-entry");
            }

            if (jQuery("#institute").hasClass("validation-failed")) {
                jQuery("#institute").removeClass("validation-failed");
                jQuery("#institute").removeClass("required-entry");
                jQuery("#institute").next(".validation-advice").hide();
            } else {
                jQuery("#institute").removeClass("required-entry");
            }
        } else {
            jQuery("#institute").AddClass("required-entry");
            jQuery("#store_city").AddClass("required-entry");
            jQuery("#store_name").AddClass("required-entry");
            jQuery("#store_name").val("");
            jQuery("#store_key").val("");
        }
    });
});

function storeLossFocus(event) {
    jQuery("#store_search_container").html("");
    jQuery("#store_search_container").hide();
}

function getInstitution() {

    var x = document.getElementsByTagName("form")[0].getAttribute("action");
    var url = x.replace("search", "searchStore");
    var store_city = document.getElementById("store_city").value;
    if (store_city != "") {
        new Ajax.Request(url, {
            method: 'POST',
            parameters: {'city': store_city},
            onSuccess: function (transport) {
                var response = transport.responseText.evalJSON();
                if (response.status == "SUCCESS") {
                    document.getElementById("institute").innerHTML = response.options;
                    if (response.count == 1) {
                        if (jQuery("#store_name").hasClass("validation-failed")) {
                            jQuery("#store_name").removeClass("validation-failed");
                            jQuery("#store_name").removeClass("required-entry");
                            jQuery("#store_name").next(".validation-advice").hide();
                        } else {
                            jQuery("#store_name").removeClass("required-entry");
                        }

                        if (jQuery("#institute").hasClass("validation-failed")) {
                            jQuery("#institute").removeClass("validation-failed");
                            jQuery("#institute").removeClass("required-entry");
                            jQuery("#institute").next(".validation-advice").hide();
                        } else {
                            jQuery("#institute").removeClass("required-entry");
                        }

                        if (jQuery("#store_city").hasClass("validation-failed")) {
                            jQuery("#store_city").removeClass("validation-failed");
                            jQuery("#store_city").removeClass("required-entry");
                            jQuery("#store_city").next(".validation-advice").hide();
                        } else {
                            jQuery("#store_city").removeClass("required-entry");
                        }
                    } else {
                        jQuery("#institute").AddClass("required-entry");
                        jQuery("#store_name").AddClass("required-entry");
                        jQuery("#store_city").AddClass("required-entry");
                    }

                } else {
                    alert(response.message);
                }
            },
            onFailure: function () {
                alert('Something went wrong...');
            }
        });
    } else {
        document.getElementById("institute").innerHTML = '<option value="">Select Store</option>';
        jQuery("#store_name").addClass("required-entry");
        jQuery("#store_city").removeClass("required-entry");
        jQuery("#institute").removeClass("required-entry");
    }
}

function SearchInstitution() {

    var x = document.getElementsByTagName("form")[0].getAttribute("action");
    var url = x.replace("search", "StoreSearch");
    var store_name = document.getElementById("store_name").value;
    if (jQuery("#store_name").hasClass("validation-failed")) {
        jQuery("#store_name").removeClass("validation-failed");
        jQuery("#store_name").next(".validation-advice").hide();
    }

    jQuery("#searchForm #store_name").addClass("store_searching");
    if (store_name != "" && store_name.length > 1) {
        new Ajax.Request(url, {
            method: 'POST',
            parameters: {'store_name': store_name},
            onSuccess: function (transport) {
                var response = transport.responseText.evalJSON();
                if (response.status == 1) {
                    jQuery("#store_search_container").html(response.storedata);
                    jQuery("#store_search_container").show();
                    jQuery("#searchForm #store_name").removeClass("store_searching");
                } else {
                    jQuery("#store_message").html(response.message);
                    jQuery("#store_key").val("");
                }
            },
            onFailure: function () {
                document.getElementsByClassName("store_message").innerHTML = "Something went wrong...";
            }
        });
    } else {
        jQuery("#store_key").val("");
        jQuery("#store_search_container").html("");
        jQuery("#store_name").focus();
        jQuery("#searchForm #store_name").removeClass("store_searching");

        if (jQuery("#store_city").val() == "") {
            jQuery("#store_city").addClass("required-entry");
            jQuery("#institute").addClass("required-entry");
        }
    }
}

function checkStoreFormData(event) {

    var store_city = jQuery("#searchForm #store_city").val();
    var institute = jQuery("#searchForm #institute").val();
    var store_name = jQuery("#searchForm #store_name").val();
    var store_key = jQuery("#searchForm #store_key").val();
    if (store_key == "" && institute == "") {
        jQuery("#searchForm #store_city").addClass("required-entry");
        jQuery("#searchForm #institute").addClass("required-entry");
        jQuery("#searchForm #store_name").addClass("required-entry");
        event.preventDefault();
    }
}